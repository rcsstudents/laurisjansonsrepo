﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RedditService.Authorization;
using RedditService.Models;
using RedditService.Models.Comments;
using RedditService.Models.Threads;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace RedditService.Services
{
    public class RedditServices : IRedditServices
    {
        private readonly string _token;
        private const string baseUrl = "https://oauth.reddit.com/";
        private readonly RedditAuthorization _redditAuthorization;

        public RedditServices()
        {
            _redditAuthorization = new RedditAuthorization();
            _token = _redditAuthorization.GetAuthorizationToken();
        }

        public List<RedditThreadsDto> GetRedditThreads(int threadCount, int commentsCount)
        {
            List<RedditThreadsDto> redditThreadsList = new List<RedditThreadsDto>();

            string getBestResult = CallRedditApi($"best?limit={threadCount}");
            RedditThreads redditThreads = JsonConvert.DeserializeObject<RedditThreads>(getBestResult);

            foreach (Models.Threads.Child data in redditThreads.data.children)
            {
                redditThreadsList.Add(new RedditThreadsDto
                {
                    Title = data.data.title,
                    Comments = GetArticleComments(data.data, commentsCount)
                });
            }

            return redditThreadsList;
        }

        private List<string> GetArticleComments(Models.Threads.Data2 data, int commentsCount)
        {
            List<string> comments = new List<string>();

            string commentsResult = CallRedditApi($"r/{data.subreddit}/comments/{data.id}?limit={commentsCount}&sort=best&depth=1");
            List<RedditComments> redditComments = JsonConvert.DeserializeObject<List<RedditComments>>(commentsResult);

            foreach(var redditComment in redditComments)
            {
                foreach (var comment in redditComment.data.children)
                {
                    if (string.IsNullOrEmpty(comment.data.body))
                        continue;

                    comments.Add(comment.data.body);
                }
            }

            return comments;
        }

        private string CallRedditApi(string api)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"{baseUrl}{api}");
            request.Method = "GET";
            request.Headers["Authorization"] = "Bearer " + _token;
            request.Headers["User-Agent"] = Credentials.ClientId;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
