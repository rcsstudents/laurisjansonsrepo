﻿using RedditService.Models;
using System.Collections.Generic;

namespace RedditService.Services
{
    public interface IRedditServices
    {
        List<RedditThreadsDto> GetRedditThreads(int threadCount, int commentsCount);
    }
}