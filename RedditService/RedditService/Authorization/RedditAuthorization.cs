﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RedditService.Authorization;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace RedditService.Services
{
    public class RedditAuthorization
    {
        private const string OauthGetMeUrl = "https://oauth.reddit.com/api/v1/me";
        private const string AccessUrl = "https://ssl.reddit.com/api/v1/access_token";

        public string GetAuthorizationToken()
        {
            string requestData = "grant_type=https://oauth.reddit.com/grants/installed_client" + "&device_id=7f5942f0-9abe-4235-954a-0cf612419ca3";
            byte[] data = new ASCIIEncoding().GetBytes(requestData);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AccessUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(Credentials.ClientId + ":" + Credentials.ClientSecret));

            WriteRequestData(data, request);

            JToken response = GetResponse(request);

            return response["access_token"].ToString();
        }

        private void WriteRequestData(byte[] data, HttpWebRequest request)
        {
            Stream stream = request.GetRequestStream();
            stream.Write(data, 0, data.Length);
            stream.Close();
        }

        private JToken GetResponse(HttpWebRequest request)
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                string result = streamReader.ReadToEnd();
                return JsonConvert.DeserializeObject<JToken>(result);
            };
        }
    }
}
