﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedditService.Models.Comments
{
    //Generated in json2csharp.com
    public class RedditComments
    {
        public string kind { get; set; }
        public Data data { get; set; }
    }

    public class MediaEmbed
    {
    }

    public class SecureMediaEmbed
    {
    }

    public class Data6
    {
        public int count { get; set; }
        public string name { get; set; }
        public string id { get; set; }
        public string parent_id { get; set; }
        public int depth { get; set; }
        public List<string> children { get; set; }
    }

    public class Child3
    {
        public string kind { get; set; }
        public Data6 data { get; set; }
    }

    public class Data5
    {
        public string modhash { get; set; }
        public object dist { get; set; }
        public List<Child3> children { get; set; }
        public object after { get; set; }
        public object before { get; set; }
    }

    public class Replies2
    {
        public string kind { get; set; }
        public Data5 data { get; set; }
    }

    public class Data4
    {
        public string subreddit_id { get; set; }
        public object approved_at_utc { get; set; }
        public int ups { get; set; }
        public object mod_reason_by { get; set; }
        public object banned_by { get; set; }
        public string author_flair_type { get; set; }
        public object removal_reason { get; set; }
        public string link_id { get; set; }
        public object author_flair_template_id { get; set; }
        public object likes { get; set; }
        public bool no_follow { get; set; }
        public Replies2 replies { get; set; }
        public List<object> user_reports { get; set; }
        public bool saved { get; set; }
        public string id { get; set; }
        public object banned_at_utc { get; set; }
        public object mod_reason_title { get; set; }
        public int gilded { get; set; }
        public bool archived { get; set; }
        public object report_reasons { get; set; }
        public string author { get; set; }
        public bool can_mod_post { get; set; }
        public bool send_replies { get; set; }
        public string parent_id { get; set; }
        public int score { get; set; }
        public string author_fullname { get; set; }
        public object approved_by { get; set; }
        public int downs { get; set; }
        public string body { get; set; }
        public bool edited { get; set; }
        public object author_flair_css_class { get; set; }
        public bool collapsed { get; set; }
        public List<object> author_flair_richtext { get; set; }
        public bool is_submitter { get; set; }
        public object collapsed_reason { get; set; }
        public string body_html { get; set; }
        public bool stickied { get; set; }
        public string subreddit_type { get; set; }
        public bool can_gild { get; set; }
        public string subreddit { get; set; }
        public object author_flair_text_color { get; set; }
        public bool score_hidden { get; set; }
        public string permalink { get; set; }
        public object num_reports { get; set; }
        public string name { get; set; }
        public double created { get; set; }
        public object author_flair_text { get; set; }
        public double created_utc { get; set; }
        public string subreddit_name_prefixed { get; set; }
        public int controversiality { get; set; }
        public int depth { get; set; }
        public object author_flair_background_color { get; set; }
        public List<object> mod_reports { get; set; }
        public object mod_note { get; set; }
        public object distinguished { get; set; }
        public int? count { get; set; }
        public List<string> children { get; set; }
    }

    public class Child2
    {
        public string kind { get; set; }
        public Data4 data { get; set; }
    }

    public class Data3
    {
        public string modhash { get; set; }
        public object dist { get; set; }
        public List<Child2> children { get; set; }
        public object after { get; set; }
        public object before { get; set; }
    }

    public class Replies
    {
        public string kind { get; set; }
        public Data3 data { get; set; }
    }

    public class Data2
    {
        public object approved_at_utc { get; set; }
        public string subreddit { get; set; }
        public string selftext { get; set; }
        public List<object> user_reports { get; set; }
        public bool saved { get; set; }
        public object mod_reason_title { get; set; }
        public int gilded { get; set; }
        public bool clicked { get; set; }
        public string title { get; set; }
        public List<object> link_flair_richtext { get; set; }
        public string subreddit_name_prefixed { get; set; }
        public bool hidden { get; set; }
        public int pwls { get; set; }
        public string link_flair_css_class { get; set; }
        public int downs { get; set; }
        public string parent_whitelist_status { get; set; }
        public bool hide_score { get; set; }
        public string name { get; set; }
        public bool quarantine { get; set; }
        public string link_flair_text_color { get; set; }
        public double upvote_ratio { get; set; }
        public object author_flair_background_color { get; set; }
        public string subreddit_type { get; set; }
        public int ups { get; set; }
        public string domain { get; set; }
        public MediaEmbed media_embed { get; set; }
        public object author_flair_template_id { get; set; }
        public bool is_original_content { get; set; }
        public string author_fullname { get; set; }
        public object secure_media { get; set; }
        public bool is_reddit_media_domain { get; set; }
        public bool is_meta { get; set; }
        public object category { get; set; }
        public SecureMediaEmbed secure_media_embed { get; set; }
        public string link_flair_text { get; set; }
        public bool can_mod_post { get; set; }
        public int score { get; set; }
        public object approved_by { get; set; }
        public string thumbnail { get; set; }
        public object edited { get; set; }
        public object author_flair_css_class { get; set; }
        public List<object> author_flair_richtext { get; set; }
        public object content_categories { get; set; }
        public bool is_self { get; set; }
        public object mod_note { get; set; }
        public double created { get; set; }
        public string link_flair_type { get; set; }
        public int wls { get; set; }
        public object banned_by { get; set; }
        public string author_flair_type { get; set; }
        public bool contest_mode { get; set; }
        public object selftext_html { get; set; }
        public object likes { get; set; }
        public object suggested_sort { get; set; }
        public object banned_at_utc { get; set; }
        public object view_count { get; set; }
        public bool archived { get; set; }
        public bool no_follow { get; set; }
        public bool is_crosspostable { get; set; }
        public bool pinned { get; set; }
        public bool over_18 { get; set; }
        public object media { get; set; }
        public bool media_only { get; set; }
        public string link_flair_template_id { get; set; }
        public bool can_gild { get; set; }
        public bool spoiler { get; set; }
        public bool locked { get; set; }
        public object author_flair_text { get; set; }
        public bool visited { get; set; }
        public object num_reports { get; set; }
        public object distinguished { get; set; }
        public string subreddit_id { get; set; }
        public object mod_reason_by { get; set; }
        public object removal_reason { get; set; }
        public string link_flair_background_color { get; set; }
        public string id { get; set; }
        public object report_reasons { get; set; }
        public string author { get; set; }
        public int num_crossposts { get; set; }
        public int num_comments { get; set; }
        public bool send_replies { get; set; }
        public object author_flair_text_color { get; set; }
        public string permalink { get; set; }
        public string whitelist_status { get; set; }
        public bool stickied { get; set; }
        public string url { get; set; }
        public int subreddit_subscribers { get; set; }
        public double created_utc { get; set; }
        public List<object> mod_reports { get; set; }
        public bool is_video { get; set; }
        public string link_id { get; set; }
        public Replies replies { get; set; }
        public string parent_id { get; set; }
        public string body { get; set; }
        public bool? collapsed { get; set; }
        public bool? is_submitter { get; set; }
        public object collapsed_reason { get; set; }
        public string body_html { get; set; }
        public bool? score_hidden { get; set; }
        public int? controversiality { get; set; }
        public int? depth { get; set; }
        public int? count { get; set; }
        public List<string> children { get; set; }
    }

    public class Child
    {
        public string kind { get; set; }
        public Data2 data { get; set; }
    }

    public class Data
    {
        public string modhash { get; set; }
        public int? dist { get; set; }
        public List<Child> children { get; set; }
        public object after { get; set; }
        public object before { get; set; }
    }
}
