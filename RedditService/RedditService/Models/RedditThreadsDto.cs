﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedditService.Models
{
    public class RedditThreadsDto
    {
        public string Title { get; set; }
        public List<string> Comments { get; set; }
    }
}
