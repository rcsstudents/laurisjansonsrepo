﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RedditService.Models;
using RedditService.Services;

namespace RedditService.Controllers
{
    [Route("api/")]
    [ApiController]
    public class RedditController : ControllerBase
    {
        private readonly IRedditServices _services;

        public RedditController(IRedditServices services)
        {
            _services = services;
        }

        [HttpGet]
        [Route("GetRedditThreads")]
        public ActionResult<List<RedditThreadsDto>> GetRedditThreads(int threadCount = 5, int commentsCount = 5)
        {
            return _services.GetRedditThreads(threadCount, commentsCount);
        }
    }
}