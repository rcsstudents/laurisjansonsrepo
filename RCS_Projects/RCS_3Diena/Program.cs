﻿using System;
using System.Collections.Generic;

namespace RCS_3Diena
{
    class Program
    {
        static void Main(string[] args)
        {
            //Diena3();

            Animal animal = new Animal();
            Person person = new Person("Lauris", 123, 123, "blue", DayOfWeekEnum.Friday);

            animal.Eat();
            animal.Drink();

            person.Eat();
            person.Drink();

            Console.ReadKey();
        }

        public static void Diena3()
        {
            List<IWalking> walkingList = new List<IWalking>();
            List<Person> personList = new List<Person>();
            List<Programmer> programmerList = new List<Programmer>();

            string turpinat = "";
            while (turpinat != "n")
            {
                Console.WriteLine("Ievadi vārdu");
                string vards = Console.ReadLine();

                Console.WriteLine("Ievadi augumu");
                string augums = Console.ReadLine();
                double augumsDouble = Convert.ToDouble(augums);

                Console.WriteLine("Ievadi svaru");
                string svars = Console.ReadLine();
                double svarsDouble = Convert.ToDouble(svars);

                Console.WriteLine("Ievadi matu krāsu");
                string matuKrasa = Console.ReadLine();

                Console.WriteLine("Ievadi dienu 1-7");
                string dayOfWeek = Console.ReadLine();
                int dayOfWeekInt = Convert.ToInt32(dayOfWeek);
                DayOfWeekEnum nedelasDiena = (DayOfWeekEnum)dayOfWeekInt;

                Person cilveks = new Person(vards, augumsDouble, svarsDouble, matuKrasa, nedelasDiena);
                personList.Add(cilveks);

                Programmer programmer = new Programmer(vards, augumsDouble, svarsDouble, matuKrasa, "C#");
                programmerList.Add(programmer);

                Animal animal = new Animal();
                animal.Eat();
                animal.Drink();

                walkingList.Add(cilveks);
                walkingList.Add(animal);

                Console.WriteLine("Turpināt? y/n");
                turpinat = Console.ReadLine();
            }

            foreach (Person person in personList)
            {
                person.WritePersonInfo();
            }

            foreach (Programmer programmer in programmerList)
            {
                programmer.WritePersonInfo();
            }

            foreach (IWalking walking in walkingList)
            {
                walking.DoWalking();
            }

            Console.ReadKey();
        }
    }
}
