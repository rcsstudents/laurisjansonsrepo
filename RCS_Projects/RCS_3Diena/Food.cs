﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_3Diena
{
    public abstract class Food
    {
        //Nesatur funkcionalitāti
        //Obligāti vajag override šo metodi

        //public abstract void DrinkAbstract();

        //Satur funkcionalitāti
        //Var override šo metodi, ja nepieciešams
        public virtual void Drink()
        {
            Console.WriteLine("I drink beer");
        }

        //Satur funkcionalitāti
        //Nevar override
        public void Eat()
        {
            Console.WriteLine("I like beer");
        }
    }
}
