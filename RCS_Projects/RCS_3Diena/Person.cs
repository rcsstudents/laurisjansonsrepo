﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_3Diena
{
    public class Person : Food, IWalking
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        public DayOfWeekEnum FavoriteWeekDay;

        public Person(string name, double height, double weight, string hairColor, DayOfWeekEnum favoriteWeekDay)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            FavoriteWeekDay = favoriteWeekDay;
        }

        public virtual void WritePersonInfo()
        {
            Console.WriteLine($"Persona {Name} ir {Height} cm augumā un sver {Weight} kg.");
            Console.WriteLine($"Matu krāsa ir {HairColor}");
            Console.WriteLine($"Miļākā nedēļas diena ir {FavoriteWeekDay}");
        }

        public void DoWalking()
        {
            Console.WriteLine("Persona nogāja 10km");
        }

        public override void Drink()
        {
            Console.WriteLine("Person drinks beer");
        }
    }
}
