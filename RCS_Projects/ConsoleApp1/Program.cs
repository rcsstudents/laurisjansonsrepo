﻿using DatuTipi;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IPerson> persons = new List<IPerson>();
            persons.Add(new Student("Sandis", "Ozols"));
            persons.Add(new Professor("Juris", "Ābols"));

            foreach(IPerson person in persons)
            {
                Console.WriteLine(person.GetHomework());
            }
            Console.ReadKey();

            //List<MyDataTypes> myDataTypesList = new List<MyDataTypes>();

            //Console.WriteLine("Izpildīt programmu? (y/n)");
            //while (Console.ReadLine() != "n")
            //{
            //    MyDataTypes myDataTypes = new MyDataTypes();
            //    //1.Bool
            //    Console.WriteLine("Ievadiet bool vērtību (true/false)");
            //    string myBoolString = Console.ReadLine();
            //    myDataTypes.MyBool = Convert.ToBoolean(myBoolString);

            //    //2.Byte
            //    Console.WriteLine("Ievadiet byte vērtību");
            //    string myByte = Console.ReadLine();
            //    bool isConverted = byte.TryParse(myByte, out byte result);
            //    if (isConverted == true)
            //    {
            //        myDataTypes.MyByte = Convert.ToByte(myByte);
            //    }

            //    //3.Short
            //    Console.WriteLine("Ievadiet short vērtību");
            //    string myShort = Console.ReadLine();
            //    myDataTypes.MyShort = Convert.ToInt16(myShort);

            //    //.....

            //    //9.Char
            //    Console.WriteLine("Ievadiet char vērtību");
            //    string myChar = Console.ReadLine();
            //    myDataTypes.MyChar = Convert.ToChar(myChar);

            //    myDataTypesList.Add(myDataTypes);
            //}

            ////for (int i= 0; i< myDataTypesList.Count; i++)

            //foreach(MyDataTypes myData in myDataTypesList)
            //{
            //    Console.WriteLine("MyBool = " + myData.MyBool);
            //}

            //Console.ReadKey();
        }
    }
}
