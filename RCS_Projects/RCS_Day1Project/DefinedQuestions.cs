﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_Day1Project
{
    public static class DefinedQuestions
    {
        public static List<QuizQuestion> quizQuestion = new List<QuizQuestion>
        {
            new QuizQuestion
            {
                Question = "Kāda ir tava mīļākā programmēšanas valoda?",
                Answer = "C#"
            },
            new QuizQuestion
            {
                Question = "Kāda ir tava mīļākā operētājsistēma?",
                Answer = "Windows"
            },
            new QuizQuestion
            {
                Question = "Kāds ir tavs mīļākais saldējums?",
                Answer = "Plombīra"
            }
        };
    }
}
