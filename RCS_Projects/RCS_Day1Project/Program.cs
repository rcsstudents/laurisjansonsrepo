﻿using System;
using System.Collections.Generic;

namespace RCS_Day1Project
{
    class Program
    {
        //
        //
        static void Main(string[] args)
        {
            List<string> userAnswers = new List<string>();
            int score = 0;


            //
            //uzdodam jautājumus
            DefinedQuestions.quizQuestion.Shuffle();
            foreach (QuizQuestion question in DefinedQuestions.quizQuestion)
            {
                Console.WriteLine(question.Question);
                string answer = Console.ReadLine();
                if(answer == question.Answer)
                {
                    score++;
                }
                userAnswers.Add(answer);
            }

            //izdrukājam atbildes
            for(int i = 0; i < userAnswers.Count; i++)
            {
                Console.WriteLine("Pareizā atbilde: "+ DefinedQuestions.quizQuestion[i].Answer);
                Console.WriteLine("Tu atbildēji: " + userAnswers[i]);
            }

            Console.WriteLine("Rezultāts ir: " + score);
            Console.WriteLine("Max punktu skaits: " + userAnswers.Count);

            Console.ReadKey();
        }
    }
}
