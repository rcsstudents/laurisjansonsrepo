﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_4Diena_AbstraktasKlases
{
    public class Professor : Person, IPerson
    {
        public Professor(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public string GetHomework()
        {
            string fullName = GetFullName();
            return $"Professor {fullName} has no homework!";
        }

        public override void GetSomething()
        {
            Console.WriteLine("Āh");
        }
    }
}
