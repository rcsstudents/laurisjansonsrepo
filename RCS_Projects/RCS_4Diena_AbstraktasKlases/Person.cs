﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS_4Diena_AbstraktasKlases
{
    public abstract class Person
    {
        public string Name;
        public string Surname;

        public string GetFullName()
        {
            return $"{Name} {Surname}";
        }

        public virtual void GetSomething()
        {
            Console.WriteLine("I dont drink food");
        }
    }
}
