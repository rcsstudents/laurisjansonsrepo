﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    internal class Uzdevums13
    {
        internal static void DoUzdevums13()
        {
            List<string> saraksts = new List<string>
            {
                "abs", "abs", "abs", "sport", "volleyball", "volleyball", "hidroelektrostacija", "rcs"
            };

            saraksts.Distinct().ToList().ForEach(s => Console.WriteLine(s));
        }
    }
}