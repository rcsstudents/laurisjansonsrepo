﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    internal class Uzdevums7
    {
        internal static void DoUzdevums7()
        {
            Console.WriteLine("Ievadiet vārdu virkni:");
            string varduVirkne = Console.ReadLine();
            List<string> vardi = varduVirkne.Split(' ').ToList();

            LielieBurtiSakums(vardi);
            MazieBurtiSakums(vardi);
            LielieBurtiBeigas(vardi);
            MazieBurtiBeigas(vardi);

            Console.ReadKey();
        }

        private static void MazieBurtiBeigas(List<string> vardi)
        {
            Console.WriteLine("5. mazie burti beigas");
            vardi
                .Where(v => v.Last().ToString().Equals(v.Last().ToString().ToLower()))
                .ToList()
                .ForEach(v => Console.WriteLine(v));
        }

        private static void LielieBurtiBeigas(List<string> vardi)
        {
            Console.WriteLine("4. Lielie Burti Beigas");
            vardi
                .Where(v => v[v.Length - 1].ToString().Equals(v[v.Length - 1].ToString().ToUpper()))
                .ToList()
                .ForEach(v => Console.WriteLine(v));
        }

        private static void MazieBurtiSakums(List<string> vardi)
        {
            Console.WriteLine("3. mazie burti Sākums");
            vardi
                .Where(v => v.First().ToString().Equals(v.First().ToString().ToLower()))
                .ToList()
                .ForEach(v => Console.WriteLine(v));
        }

        private static void LielieBurtiSakums(List<string> vardi)
        {
            Console.WriteLine("2. Lielie Burti Sākums");
            vardi
                .Where(v => v[0].ToString().Equals(v[0].ToString().ToUpper()))
                .ToList()
                .ForEach(v => Console.WriteLine(v));
        }
    }
}