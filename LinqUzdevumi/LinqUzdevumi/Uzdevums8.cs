﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    internal class Uzdevums8
    {
        internal static void DoUzdevums8()
        {
            List<string> saraksts = new List<string>
            {
                "vards1", "vards2", "vards3", "kaķis", "suns", "pele"
            };

            string sarakstaElementi = string.Join(">*<", saraksts);
            Console.WriteLine($"<{sarakstaElementi}>");
        }
    }
}