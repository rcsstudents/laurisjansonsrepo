﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uzdevums1
{
    class Student
    {
        public int Id;
        public string Name;
        public byte Grade;

        public Student(int id, string name, byte grade)
        {
            Id = id;
            Name = name;
            Grade = grade;
        }
    }
}
