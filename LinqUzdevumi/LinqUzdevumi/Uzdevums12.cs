﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    internal class Uzdevums12
    {
        internal static void DoUzdevums12()
        {
            List<string> saraksts = new List<string>
            {
                "abs", "sport", "volleyball", "hidroelektrostacija", "rcs"
            };
            //OrderBy
            Console.WriteLine("\nOrderBy:");
            saraksts = saraksts
                .OrderBy(s => s)
                .ToList();

            WriteJoinedList(saraksts);

            //OrderByDescending
            Console.WriteLine("\nOrderByDescending:");
            saraksts = saraksts
                .OrderByDescending(s => s)
                .ToList();

            WriteJoinedList(saraksts);

            Console.WriteLine("\nOrderBy length:");
            saraksts = saraksts
                .OrderBy(s => s.Length)
                .ToList();

            WriteJoinedList(saraksts);

            Console.WriteLine("\nOrderBy length DESC:");
            saraksts = saraksts
                .OrderByDescending(s => s.Length)
                .ToList();

            WriteJoinedList(saraksts);

            Console.WriteLine("\n>= 4:");
            WriteJoinedList(saraksts.Where(s => s.Length >= 4).ToList());

            Console.WriteLine("\n<= 6:");
            WriteJoinedList(saraksts.Where(s => s.Length <= 6).ToList());
        }

        private static void WriteJoinedList(List<string> saraksts)
        {
            string joined = string.Join(Environment.NewLine, saraksts);
            Console.WriteLine(joined);
        }
    }
}