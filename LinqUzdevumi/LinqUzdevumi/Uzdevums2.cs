﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    public class Uzdevums2
    {
        public static void DoUzdevums2()
        {
            List<int> saraksts = new List<int> { 3, 9, 2, 8, 6, 5 };

            saraksts
                .Select(s => $"{s} * {s} = {s * s}")
                .ToList()
                .ForEach(s => Console.WriteLine(s));

            Console.Write("Saraksta summa = ");
            int summa = saraksts.Sum();
            Console.WriteLine(summa);
        }
    }
}