﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    internal class Uzdevums9
    {
        internal static void DoUzdevums9()
        {
            List<Student> students = new List<Student>();
            AddStudents(students);

            AugstakaAtzimeStudentiem(students);
            ZemakaAtzimeStudentiem(students);

            Teicamnieki(students);

            students.Where(s => s.Grade < 4)
                .ToList()
                .ForEach(s => Console.WriteLine($"Atzīme zem 4 " +
                $"ir studentam {s.Name} ar Id={s.Id} un atzīmi {s.Grade}"));

            Console.WriteLine();
        }

        private static void Teicamnieki(List<Student> students)
        {
            students.Where(s => s.Grade > 8)
                .ToList()
                .ForEach(s => Console.WriteLine($"Atzīme virs 8 " +
                $"ir studentam {s.Name} ar Id={s.Id} un atzīmi {s.Grade}"));
        }

        private static void ZemakaAtzimeStudentiem(List<Student> students)
        {
            Student lowestGradeStudent =
                            students.OrderBy(s => s.Grade).First();

            int zemakaAtzime = lowestGradeStudent.Grade;

            students.Where(s => s.Grade == zemakaAtzime)
                .ToList()
                .ForEach(s => Console.WriteLine($"Zemākā atzīme {s.Grade} " +
                $"ir studentam {s.Name} ar Id={s.Id}"));
        }

        private static void AugstakaAtzimeStudentiem(List<Student> students)
        {
            Student highGradeStudent =
                            students.OrderByDescending(s => s.Grade).First();

            int augstakaAtzime = highGradeStudent.Grade;

            students.Where(s => s.Grade == augstakaAtzime)
                .ToList()
                .ForEach(s => Console.WriteLine($"Augstākā atzīme {s.Grade} " +
                $"ir studentam {s.Name} ar Id={s.Id}"));
        }

        public static void AddStudents(List<Student> students)
        {
            students.Add(new Student(1, "Lauris", 10));
            students.Add(new Student(2, "Jānis", 2));
            students.Add(new Student(3, "Toms", 6));
            students.Add(new Student(4, "Kārlis", 9));
            students.Add(new Student(5, "Dāvis", 2));
            students.Add(new Student(6, "Nauris", 3));
        }
    }
}