﻿using Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataManager.ManageData
{
    public class DatabaseManager
    {
        string connectionString = "server=localhost;database=universitydatabase;uid=root;pwd=root;";

        public void CreateConnection()
        { 
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                Console.WriteLine("Success");

                ReadStudentsFromDatabase(connection);
                UpdateStudentInDatabase(connection);
                DeleteStudentFromDatabase(connection);

                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        public void DeleteStudentFromDatabase(MySqlConnection connection)
        {
            string sql = "delete from students where id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", 5);
            command.ExecuteNonQuery();
        }

        public void UpdateStudentInDatabase(MySqlConnection connection)
        {
            string sql = "Update students set `name` = @name, surname = @surname where id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@name", "Jānis");
            command.Parameters.AddWithValue("@surname", "Bērziņš");
            command.Parameters.AddWithValue("@id", 1);
            command.ExecuteNonQuery();
        }

        public void InsertInDatabase(Student student)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();

                InsertStudentToDatabase(connection, student);

                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void InsertStudentToDatabase(MySqlConnection connection, Student student)
        {
            string sql = "Insert into students (`name`, surname, university_id) Values (@name, @surname, @university_id)";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@name", student.Name);
            command.Parameters.AddWithValue("@surname", student.Surname);
            command.Parameters.AddWithValue("@university_id", student.UniversityId);
            command.ExecuteNonQuery();
        }

        public List<Student> ReadFromDatabase()
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                Console.WriteLine("Success");

                List<Student> students = ReadStudentsFromDatabase(connection);

                connection.Close();

                return students;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<Student> ReadStudentsFromDatabase(MySqlConnection connection)
        {
            List<Student> studentList = new List<Student>();

            string sql = "select * from students";
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandText = sql;

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Student student = new Student();
                    student.Id = reader.GetInt32("id");
                    student.Name = reader.GetString("name");
                    student.Surname = reader.GetString("surname");
                    //student.UniversityId = reader.GetInt32("university_id");

                    studentList.Add(student);
                }
            }
            return studentList;
        }
    }
}
