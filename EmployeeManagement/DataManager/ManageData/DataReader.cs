﻿using Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DataManager.ManageData
{
    public class DataReader
    {
        private readonly string FileName;

        public DataReader(string fileName)
        {
            FileName = fileName;
        }
        
        public List<T> ReadData<T>()
        {
            using (StreamReader reader = new StreamReader(FileName))
            {
                string dataFromFile = reader.ReadToEnd();
                List<T> data = JsonConvert.DeserializeObject<List<T>>(dataFromFile);
                return data ?? new List<T>();
            }
        }

        public List<Student> ReadStudentFromDB()
        {
            DatabaseManager dataManager = new DatabaseManager();
            return dataManager.ReadFromDatabase();
        }
    }
}
