﻿using Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DataManager.ManageData
{
    public class DataWriter
    {
        private readonly string FileName;

        public DataWriter(string fileName)
        {
            FileName = fileName;
        }

        public void WriteData<T>(List<T> data)
        {
            using (StreamWriter writer = new StreamWriter(FileName))
            {
                string dataJson = JsonConvert.SerializeObject(data);
                writer.WriteLine(dataJson);
            }
        }

        public void InsertStudent(Student student)
        {
            DatabaseManager dataManager = new DatabaseManager();
            dataManager.InsertInDatabase(student);
        }
    }
}
