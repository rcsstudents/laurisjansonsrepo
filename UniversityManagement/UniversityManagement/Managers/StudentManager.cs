﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.Managers
{
    public class StudentManager : IManager
    {
        List<Student> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);
            Students = reader.ReadStudentFromDB();
        }

        public void Create()
        {
            Student student = new Student();
            Console.WriteLine("Create student.Id:");
            student.Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Create student.Name:");
            student.Name = Console.ReadLine();
            Console.WriteLine("Create student.Surname:");
            student.Surname = Console.ReadLine();
            Console.WriteLine("Create student.UniversityId:");
            student.UniversityId = Convert.ToInt32(Console.ReadLine());

            Students.Add(student);
            writer.InsertStudent(student);
        }

        public void Delete()
        {
            Console.WriteLine("Delete student.Id:");
            int id = Convert.ToInt32(Console.ReadLine());
            Students = Students.Where(u => u.Id != id).ToList();
            writer.WriteData(Students);
        }

        public void Read()
        {
            UniversityManager universityManager = new UniversityManager();
            foreach (Student student in Students)
            {
                Console.WriteLine($"Student {student.Id} {student.Name} {student.Surname} studies in:");
                University studentsUniversity = universityManager.Universities
                    .FirstOrDefault(u => u.Id == student.UniversityId);
                if(studentsUniversity != null)
                {
                    Console.WriteLine($"{studentsUniversity.Id} {studentsUniversity.Name}");
                }
                else
                {
                    Console.WriteLine($"Whoops. No University found by Id {student.UniversityId}");
                }
            }
        }

        public void Update()
        {
            Console.WriteLine("Update By student.Id:");
            int id = Convert.ToInt32(Console.ReadLine());
            Student student = Students.FirstOrDefault(u => u.Id == id);
            if (student == null)
            {
                Console.WriteLine($"Students ar Id={1} neeksistē");
                return;
            }

            Console.WriteLine("Updated student.Name:");
            student.Name = Console.ReadLine();
        }
    }
}
