﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.Managers
{
    public class UniversityManager : IManager
    {
        public List<University> Universities;
        DataReader reader;
        DataWriter writer;

        public UniversityManager()
        {
            reader = new DataReader(DataConfiguration.UniversityData);
            writer = new DataWriter(DataConfiguration.UniversityData);
            Universities = reader.ReadData<University>();
        }

        public void Create()
        {
            University university = new University();
            Console.WriteLine("Create university.Id:");
            university.Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Create university.Name:");
            university.Name = Console.ReadLine();

            Universities.Add(university);
            writer.WriteData(Universities);
        }

        public void Delete()
        {
            Console.WriteLine("Delete university.Id:");
            int id = Convert.ToInt32(Console.ReadLine());
            Universities = Universities.Where(u => u.Id != id).ToList();
            writer.WriteData(Universities);
        }

        public void Read()
        {
            foreach (University university in Universities)
            {
                Console.WriteLine($"{university.Id} {university.Name}");
            }
        }

        public void Update()
        {
            Console.WriteLine("Update By university.Id:");
            int id = Convert.ToInt32(Console.ReadLine());
            University university = Universities.FirstOrDefault(u => u.Id == id);
            if (university == null)
            {
                Console.WriteLine($"Universitāte ar Id={1} neeksistē");
                return;
            }

            Console.WriteLine("Updated university.Name:");
            university.Name = Console.ReadLine();
        }
    }
}
