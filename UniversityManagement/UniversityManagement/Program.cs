﻿using DataManager.ManageData;
using System;
using UniversityManagement.Managers;

namespace UniversityManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            //DatabaseManager dbManager = new DatabaseManager();
            //dbManager.CreateConnection();
            //return;

            string turpinat = "";
            while (turpinat != "n")
            {
                Console.WriteLine("Welcome to UniversityManagement");
                Console.WriteLine("Enter 0=(Student) or 1=(University):");
                string entityTypeString = Console.ReadLine();
                int entityType = Convert.ToInt32(entityTypeString);

                IManager manager = GetEntityManager((EntityType)entityType);

                DoDataActions(manager);

                Console.WriteLine("Vai turpināt? (y/n)");
                turpinat = Console.ReadLine();
                Console.Clear();
            }
        }

        public static IManager GetEntityManager(EntityType entityType)
        {
            IManager manager = null;

            switch (entityType)
            {
                case EntityType.Student:
                    manager = new StudentManager();
                    break;
                case EntityType.University:
                    manager = new UniversityManager();
                    break;
                default:
                    Console.WriteLine($"Cannot find entity with id={(int)entityType}");
                    break;
            }

            return manager;
        }

        private static void DoDataActions(IManager manager)
        {
            Console.WriteLine("Enter 0=(Create) or 1=(Read) or 2=(Update) or 3=(Delete):");
            string dataActionString = Console.ReadLine();
            int dataAction = Convert.ToInt32(dataActionString);

            switch ((DataAction)dataAction)
            {
                case DataAction.Create:
                    manager.Create();
                    break;
                case DataAction.Read:
                    manager.Read();
                    break;
                case DataAction.Update:
                    manager.Update();
                    break;
                case DataAction.Delete:
                    manager.Delete();
                    break;
                default:
                    Console.WriteLine($"Invalid data action: {dataAction}");
                    break;
            }
        }
    }
}
